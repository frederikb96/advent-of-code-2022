
def main():
    file = open("input.txt", "r")
    line = file.readline()

    h = []
    while line:
        h.append([])
        for l in line[:-1]:
            h[-1].append(int(l))

        line = file.readline()

    file.close()
    size_i = len(h)
    size_j = len(h[0])

    res = 0
    for i in range(1, size_i - 1):
        for j in range(1, size_j - 1):
            l = 1
            r = 1
            t = 1
            b = 1
            while j-l-1 >= 0 and h[i][j] > h[i][j-l]:
                l += 1
            while j+r+1 < size_j and h[i][j] > h[i][j+r]:
                r += 1
            while i-t-1 >= 0 and h[i][j] > h[i-t][j]:
                t += 1
            while i+b+1 < size_i and h[i][j] > h[i+b][j]:
                b += 1
            current = l * r * t * b
            if current > res:
                res = current

    print(res)


if __name__ == "__main__":
    main()
