

def main():
    file = open("input.txt", "r")

    line = file.readline()
    lines = []
    while line != "\n":
        lines.append(line)
        line = file.readline()
    lines.reverse()
    max_stacks = int(lines[0][-2])

    stacks = [[] for i in range(max_stacks)]
    for line in lines[1:]:
        for i in range(max_stacks):
            current = line[1+i*4]
            if current != " ":
                stacks[i].append(current)

    line = file.readline()
    while line:
        current = line[5:].replace("\n", " ").split(" ")
        n_times = int(current[0])
        source = int(current[2])
        target = int(current[4])
        for i in range(n_times):
            stacks[target-1].append(stacks[source-1].pop())
        line = file.readline()

    file.close()

    for i in range(max_stacks):
        print(stacks[i].pop())


if __name__ == "__main__":
    main()
