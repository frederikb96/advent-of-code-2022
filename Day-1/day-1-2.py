

def main():
    file = open("input.txt", "r")

    line = file.readline()
    max_pkg = [0, 0, 0]
    current = 0
    while line:
        if line != '\n' and line != '':
            current += int(line[:-1])
        else:
            if current > max_pkg[0]:
                if current > max_pkg[1]:
                    if current > max_pkg[2]:
                        max_pkg = max_pkg[1:] + [current]
                    else:
                        max_pkg = [max_pkg[1], current, max_pkg[2]]
                else:
                    max_pkg = [current] + max_pkg[1:]
            current = 0

        line = file.readline()

    file.close()
    print(max_pkg[0] + max_pkg[1] + max_pkg[2])


if __name__ == "__main__":
    main()
