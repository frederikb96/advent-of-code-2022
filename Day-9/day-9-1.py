

def main():
    file = open("input.txt", "r")
    line = file.readline()
    a = {}
    h = [0, 0]
    t = [0, 0]
    def move_tail():
        dx = h[0] - t[0] #+:head right of tail | -:head left of tail
        dy = h[1] - t[1] #+:head under tail | -:head above tail
        if dx > 1 or dy > 1 or dx < -1 or dy < -1:
            if dx == -2:
                if dy == -1:
                    t[0] -= 1
                    t[1] -= 1
                elif dy == 0:
                    t[0] -= 1
                elif dy == 1:
                    t[0] -= 1
                    t[1] += 1
            elif dx == -1:
                if dy == -2:
                    t[0] -= 1
                    t[1] -= 1
                elif dy == 2:
                    t[0] -= 1
                    t[1] += 1
            elif dx == 0:
                if dy == -2:
                    t[1] -= 1
                elif dy == 2:
                    t[1] += 1
            elif dx == +1:
                if dy == -2:
                    t[0] += 1
                    t[1] -= 1
                elif dy == 2:
                    t[0] += 1
                    t[1] += 1
            elif dx == +2:
                if dy == -1:
                    t[0] += 1
                    t[1] -= 1
                elif dy == 0:
                    t[0] += 1
                elif dy == 1:
                    t[0] += 1
                    t[1] += 1
        a[t[0],t[1]] = True
    while line:
        line = line.replace("\n", "").split(" ")
        d = line[0]
        v = int(line[1])
        if d == "L":
            for _ in range(v):
                h[0] -= 1
                move_tail()
        elif d == "R":
            for _ in range(v):
                h[0] += 1
                move_tail()
        elif d == "U":
            for _ in range(v):
                h[1] -= 1
                move_tail()
        elif d == "D":
            for _ in range(v):
                h[1] += 1
                move_tail()

        line = file.readline()

    print(len(a))
    file.close()


if __name__ == "__main__":
    main()
