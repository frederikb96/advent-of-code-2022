

def main():
    file = open("input.txt", "r")
    line = file.readline()
    file.close()

    for i in range(len(line)):
        if i >= 3:
            chars = [False for n in range(150)]
            for m in range(4):
                chars[int(ord(line[i-m]))] = True
            if sum(chars) == 4:
                print(i + 1)
                break


if __name__ == "__main__":
    main()
