

def dupl(a, b, c):

    a_bool = [False for i in range(256)]
    a_b_bool = [False for i in range(256)]

    for obj in a:
        a_bool[ord(obj)] = True
    for obj in b:
        if a_bool[ord(obj)]:
            a_b_bool[ord(obj)] = True
    for obj in c:
        if a_b_bool[ord(obj)]:
            return obj


def prio(obj):
    p = ord(obj)
    if p >= 97:
        return p - 96
    else:
        return p - 64 + 26


def main():
    file = open("input.txt", "r")

    line1 = file.readline()
    line2 = file.readline()
    line3 = file.readline()
    res = 0
    while line3:

        obj = dupl(line1[:-1], line2[:-1], line3[:-1])
        p = prio(obj)
        res += p

        line1 = file.readline()
        line2 = file.readline()
        line3 = file.readline()

    file.close()
    print(res)


if __name__ == "__main__":
    main()
