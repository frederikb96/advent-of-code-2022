

def get_win(a, b):
    if b == 'X':
        b = 'A'
    if b == 'Y':
        b = 'B'
    if b == 'Z':
        b = 'C'

    if a == b:
        return 3

    if a == 'A':
        if b == 'B':
            return 6
        else:
            return 0
    if a == 'B':
        if b == 'A':
            return 0
        else:
            return 6
    if a == 'C':
        if b == 'A':
            return 6
        else:
            return 0

def get_object(b):
    if b == 'X':
        return 1
    if b == 'Y':
        return 2
    if b == 'Z':
        return 3

def main():
    file = open("input.txt", "r")

    line = file.readline()
    sum = 0
    while line:
        a = line[0]
        b = line[2]
        sum += get_win(a, b) + get_object(b)
        line = file.readline()

    file.close()
    print(sum)


if __name__ == "__main__":
    main()
