
class Node:
    def __init__(self, parent, size):
        self.parent = parent
        self.size = size
        self.child = []
        self.checked = False

def main():
    file = open("input.txt", "r")
    line = file.readline()
    current = Node(None, 0)
    while line:
        if line.__contains__("$ cd .."):
            current = current.parent
        elif line.__contains__("$ cd "):
            new = Node(current, 0)
            current.child.append(new)
            current = new
        elif line.__contains__("$ ls") or line.__contains__("dir "):
            a = 1
        else:
            current.size += int(line.split(" ")[0])
        line = file.readline()
    file.close()

    res = 0
    while current is not None:
        all_children_checked = True
        for child in current.child:
            if not child.checked:
                child.checked = True
                current = child
                all_children_checked = False
                break
        if not all_children_checked:
            continue
        for child in current.child:
            current.size += child.size
        if current.size <= 100000:
            res += current.size
        current.checked = True
        current = current.parent

    print(res)


if __name__ == "__main__":
    main()
