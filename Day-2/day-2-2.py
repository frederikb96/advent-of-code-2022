

def get_points(a, b):
    
    points = 0
    if b == 'X':
        points = 0
    elif b == 'Y':
        points = 3
    elif b == 'Z':
        points = 6
    
    if a == 'A':
        if b == 'X':
            return points + 3
        if b == 'Y':
            return points + 1
        if b == 'Z':
            return points + 2
    if a == 'B':
        if b == 'X':
            return points + 1
        if b == 'Y':
            return points + 2
        if b == 'Z':
            return points + 3
    if a == 'C':
        if b == 'X':
            return points + 2
        if b == 'Y':
            return points + 3
        if b == 'Z':
            return points + 1

def main():
    file = open("input.txt", "r")

    line = file.readline()
    sum = 0
    while line:
        a = line[0]
        b = line[2]
        sum += get_points(a,b)
        line = file.readline()

    file.close()
    print(sum)


if __name__ == "__main__":
    main()
