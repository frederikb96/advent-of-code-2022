

def dupl(line):
    length = int(len(line) / 2)
    line_a = sorted(line[:length])
    line_b = sorted(line[length:])

    l = 0
    r = 0
    while True:
        if line_a[l] < line_b[r]:
            l += 1
        elif line_a[l] == line_b[r]:
            return line_a[l]
        else:
            r += 1


def prio(obj):
    p = ord(obj)
    if p >= 97:
        return p - 96
    else:
        return p - 64 + 26


def main():
    file = open("input.txt", "r")

    line = file.readline()
    res = 0
    while line:

        obj = dupl(line[:-1])
        p = prio(obj)
        res += p

        line = file.readline()

    file.close()
    print(res)


if __name__ == "__main__":
    main()
