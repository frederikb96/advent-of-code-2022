

def main():
    file = open("input.txt", "r")

    line = file.readline()
    max_pkg = 0
    current = 0
    index = 1
    index_max= 1
    while line:
        line = file.readline()
        index += 1
        if line == '\n' or line == '':
            current = 0
            continue
        current += int(line[:-1])
        if current > max_pkg:
            index_max = index
            max_pkg = current

    file.close()
    print(max_pkg)
    print(index_max)


if __name__ == "__main__":
    main()
