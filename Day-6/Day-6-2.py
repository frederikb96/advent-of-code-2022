

def main():
    file = open("input.txt", "r")
    line = file.readline()
    file.close()

    for i in range(len(line)):
        if i >= 13:
            chars = [False for n in range(150)]
            for m in range(14):
                chars[int(ord(line[i-m]))] = True
            if sum(chars) == 14:
                print(i + 1)
                break


if __name__ == "__main__":
    main()
