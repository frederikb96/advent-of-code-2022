

def main():
    file = open("input.txt", "r")

    line = file.readline()
    res = 0
    while line:

        parts = line.split(",")
        l1 = int(parts[0].split("-")[0])
        r1 = int(parts[0].split("-")[1])
        l2 = int(parts[1].split("-")[0])
        r2 = int(parts[1].split("-")[1][:-1])

        if l1 <= l2 and r1 >= r2:
            res += 1
        elif l2 <= l1 and r2 >= r1:
            res += 1

        line = file.readline()

    file.close()
    print(res)


if __name__ == "__main__":
    main()
