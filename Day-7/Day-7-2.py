
class Node:
    def __init__(self, parent, size):
        self.parent = parent
        self.size = size
        self.child = []
        self.checked = False

def node_size(current, free_up):
    res = current.size
    for child in current.child:
        res_child = node_size(child, free_up)
        if free_up < res_child < res:
            res = res_child
    return res

def main():
    file = open("input.txt", "r")
    line = file.readline()
    current = Node(None, 0)
    root = current
    while line:
        if line.__contains__("$ cd .."):
            current = current.parent
        elif line.__contains__("$ cd "):
            new = Node(current, 0)
            current.child.append(new)
            current = new
        elif line.__contains__("$ ls") or line.__contains__("dir "):
            a = 1
        else:
            current.size += int(line.split(" ")[0])
        line = file.readline()
    file.close()

    while current is not None:
        all_children_checked = True
        for child in current.child:
            if not child.checked:
                child.checked = True
                current = child
                all_children_checked = False
                break
        if not all_children_checked:
            continue
        for child in current.child:
            current.size += child.size
        current.checked = True
        current = current.parent

    free = 70000000 - root.child[0].size
    free_up = 30000000 - free
    res = node_size(root.child[0], free_up)
    print(res)


if __name__ == "__main__":
    main()
