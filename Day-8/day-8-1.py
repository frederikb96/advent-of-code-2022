
def main():
    file = open("input.txt", "r")
    line = file.readline()

    h = []
    while line:
        h.append([])
        for l in line[:-1]:
            h[-1].append(int(l))

        line = file.readline()

    file.close()

    size_i = len(h)
    size_j = len(h[0])

    hl = [[-1 for _ in range(size_j)] for _ in range(size_i)]
    hr = [[-1 for _ in range(size_j)] for _ in range(size_i)]
    ht = [[-1 for _ in range(size_j)] for _ in range(size_i)]
    hb = [[-1 for _ in range(size_j)] for _ in range(size_i)]
    for i in range(size_i):
        max_h = -1
        for j in range(size_j):
            if h[i][j] > max_h:
                max_h = h[i][j]
            hl[i][j] = max_h

        max_h = -1
        for j in range(size_j - 1, -1, -1):
            if h[i][j] > max_h:
                max_h = h[i][j]
            hr[i][j] = max_h

    for j in range(size_j):
        max_h = -1
        for i in range(size_i):
            if h[i][j] > max_h:
                max_h = h[i][j]
            ht[i][j] = max_h

        max_h = -1
        for i in range(size_i - 1, -1, -1):
            if h[i][j] > max_h:
                max_h = h[i][j]
            hb[i][j] = max_h

    # Solution
    res = 2 * size_i + 2 * (size_j - 2)
    for i in range(1, size_i - 1):
        for j in range(1, size_j - 1):
            if h[i][j] > hl[i][j - 1] or h[i][j] > hr[i][j + 1] or h[i][j] > ht[i - 1][j] \
                or h[i][j] > hb[i + 1][j]:
                res += 1
    print(res)


if __name__ == "__main__":
    main()
